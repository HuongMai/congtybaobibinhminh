﻿document.addEventListener("DOMContentLoaded", function (event) {
    loadStringNews();
});

function loadStringNews() {
    var max = 200;

    var content = document.getElementsByClassName('news-detail');
    for (i = 0; i < content.length; i++) {
        if (content[i].textContent.length > max) {
            content[i].innerHTML = content[i].textContent.substring(0, max) + "...";
        };
    };
}

