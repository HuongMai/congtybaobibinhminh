﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Unideli.Controllers
{
    public class FactoryController : Controller
    {
        // GET: Factory
        public ActionResult Index()
        {
            ViewBag.title = "Nhà Máy";
            return View("~/Template/" + TemplateConfig.TemplateName + "/Pages/Static/Factory.cshtml");
        }
    }
}