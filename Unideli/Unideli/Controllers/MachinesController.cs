﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Unideli.Controllers
{
    public class MachinesController : Controller
    {
        // GET: Machines
        public ActionResult Index()
        {
            ViewBag.title = "Máy móc";
            return View("~/Template/" + TemplateConfig.TemplateName + "/Pages/Static/Machines.cshtml");
        }
    }
}