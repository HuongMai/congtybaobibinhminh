﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataService.Domain;
using DataService.ViewModels;
using Unideli.Models;
using static Unideli.Ultils.Enums;

namespace Unideli.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Bao Bi Binh Minh";
            var domain = new TVFaceDomain();
            var voteList = domain.GetAllBlogActivebyType((int)BlogTypeEnum.vote, TemplateConfig.BrandID);
            return View("~/Template/" + TemplateConfig.TemplateName + "/Pages/Static/Home.cshtml", voteList);
        }
        public ActionResult AboutUs()
        {

            return View();
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Payment()
        {
            return View("~/Template/" + TemplateConfig.TemplateName + "/Pages/ECom/Payment.cshtml");
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View("~/Template/" + TemplateConfig.TemplateName + "/Pages/Static/Contact.cshtml");
        }

        public ActionResult Blog()
        {   
            ViewBag.Message = "Your contact page.";

            return View("~/Template/" + TemplateConfig.TemplateName + "/Pages/CMS/Blog.cshtml");
        }
    }
}