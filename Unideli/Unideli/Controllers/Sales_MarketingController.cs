﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Unideli.Controllers
{
    public class Sales_MarketingController : Controller
    {
        // GET: Sales_Marketing
        public ActionResult Index()
        {
            ViewBag.title = "nhân viên slale-marketing";
            return View("~/Template/" + TemplateConfig.TemplateName + "/Pages/Static/Sales_Marketing.cshtml");
        }
    }
}