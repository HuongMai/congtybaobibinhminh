﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Unideli.Controllers
{
    public class QualityController : Controller
    {
        // GET: Quality
        public ActionResult Index()
        {
            ViewBag.title = "Chất lượng";
            return View("~/Template/" + TemplateConfig.TemplateName + "/Pages/Static/Quality.cshtml");
        }
    }
}