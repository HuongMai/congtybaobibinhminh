﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Unideli.Controllers
{
    public class RecruitmentController : Controller
    {
        // GET: Recruitment
        public ActionResult Index()
        {
            ViewBag.title = "nhân viên KCS";
            return View("~/Template/" + TemplateConfig.TemplateName + "/Pages/Static/Recruitment.cshtml");
        }
    }
}