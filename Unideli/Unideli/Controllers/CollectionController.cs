﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Unideli.Models;
using DataService.ViewModels;
using System.Web.Script.Serialization;
using DataService.Domain;

namespace Unideli.Controllers
{
    public class CollectionController : Controller
    {
        // GET: Category
        public ActionResult Index()
        {
            var domain = new EcomDomain();
            //var productApi = new ProductApi();
            //var productCategoryApi = new ProductCategoryApi();
            //var productCollectionApi = new ProductCollectionApi();
            var collectionView = new CollectionViewModel();
            //var productCollectionItemMappingApi = new ProductCollectionItemMappingApi();
            var listCollection = domain.GetCollection();
            var index = 0;
            foreach (var collections in listCollection)
            {
                var listProduct = domain.GetCollectionItemById(collections.Id);

                //var listProduct = productApi.GetListProductByCatId(collections.Id);
                var collectionViewModel = new SpecialProductCollectionViewModel();
                collectionViewModel.id = collections.Id;
                collectionViewModel.name = collections.Name;
                collectionViewModel.description = collections.Description;
                collectionViewModel.seoName = collections.SEO;
                collectionViewModel.seoDescription = collections.SEODescription;
                collectionViewModel.seoKeyWord = collections.SEOKeyword;
                var tmp = Request.Url.AbsoluteUri.Split('/');
                string seoName = (string)tmp.GetValue(tmp.Length - 1);
                //if (index == 0)
                //{
                //    collectionViewModel.defaultSelected = true;
                //    index++;
                //}
                //else
                //{
                //    collectionViewModel.defaultSelected = false;
                //}
                if (seoName.Equals("danh-muc"))
                {
                    if (index == 0)
                    {
                        collectionViewModel.defaultSelected = true;
                        index++;
                    }
                    else
                    {
                        collectionViewModel.defaultSelected = false;
                    }
                }
                else if (collectionViewModel.seoName.Equals(seoName))
                {
                    collectionViewModel.defaultSelected = true;
                }
                else
                {
                    collectionViewModel.defaultSelected = false;
                }

                foreach (var products in listProduct)
                {
                    var product = domain.GetProductById(products.ProductId);
                    if (product.Active == true)
                    {
                        var productViewModel = new SpecialProductViewModel();
                        productViewModel.productID = product.ProductID;
                        productViewModel.productName = product.ProductName;
                        productViewModel.picURL = product.PicURL;
                        productViewModel.description = product.Description;
                        productViewModel.catID = product.CatID;
                        productViewModel.price = product.Price;
                        productViewModel.seoName = product.SeoName;
                        productViewModel.seoDecription = product.SeoDescription;
                        productViewModel.seoKeyWord = product.SeoKeyWords;
                        productViewModel.url = "/daily-product/" + product.SeoName;
                        collectionViewModel.ListProduct.Add(productViewModel);
                    }
                }
                collectionView.ListCollection.Add(collectionViewModel);
            }
            return View("~/Template/" + TemplateConfig.TemplateName + "/Pages/ECom/Collection.cshtml", collectionView);
        }

        // GET: Extra Product By Category ID
        public string GetExtraProduct(int ParentCatId)
        {
            //var productApi = new ProductApi();
            //var productCategoryApi = new ProductCategoryApi();
            var domain = new EcomDomain();
            var collectionView = new CollectionViewModel();
            var extraProdcutCategory = domain.GetAllCategory().Where(q => q.ParentCateId == ParentCatId).FirstOrDefault();
            var listProduct = new List<ProductViewModel>();
            if (extraProdcutCategory != null)
            {
                listProduct = domain.GetListProductByCatId(extraProdcutCategory.CateID).ToList();
            }
            var json = new JavaScriptSerializer().Serialize(listProduct);
            return json;
        }
    }            
}