﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Unideli.Controllers
{
    public class PageController : Controller
    {
        // GET: Page
        public ActionResult GetPage(string pageName)
        {
            string pageUrl = "/Pages/Static/AboutUs.cshtml";
            return View("~/Template/" + TemplateConfig.TemplateName + pageUrl);
        }
    }
}