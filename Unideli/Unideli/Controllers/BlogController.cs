﻿using DataService.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static Unideli.Ultils.Enums;

namespace Unideli.Controllers
{
    public class BlogController : Controller
    {
        // GET: Blog
        public ActionResult Index()
        {
            ViewBag.title = "Tin tức";
            var domain = new TVFaceDomain();
            var newsList = domain.GetAllBlogActivebyType((int)BlogTypeEnum.News, TemplateConfig.BrandID);
            return View("~/Template/" + TemplateConfig.TemplateName + "/Pages/CMS/News.cshtml", newsList);
        }
        public ActionResult BlogDetail(string seoName)
        {
            ViewBag.title = "Nội dung tin tức";
            return View("~/Template/" + TemplateConfig.TemplateName + "/Pages/CMS/News_Details.cshtml");
        }
    }
}