﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Unideli.Controllers
{
    public class EngineeringController : Controller
    {
        // GET: Engineering
        public ActionResult Index()
        {
            ViewBag.title = "nhân viên Kỹ Thuật";
            return View("~/Template/" + TemplateConfig.TemplateName + "/Pages/Static/Engineering.cshtml");
        }
    }
}