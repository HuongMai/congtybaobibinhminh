﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Unideli.Controllers
{
    public class MAterialsController : Controller
    {
        // GET: MAterials
        public ActionResult Index()
        {
            ViewBag.title = "Nguyên vật liệu";
            return View("~/Template/" + TemplateConfig.TemplateName + "/Pages/Static/Materials.cshtml");
        }
    }
}