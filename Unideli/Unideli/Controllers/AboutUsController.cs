﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Unideli.Controllers
{
    public class AboutUsController : Controller
    {
        // GET: AboutUs
        public ActionResult Index()
        {
            ViewBag.title = "Giới thiệu";
            return View("~/Template/" + TemplateConfig.TemplateName + "/Pages/Static/AboutUs.cshtml");
        }
    }
}