﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Unideli.Controllers
{
    public class GetDataController : Controller
    {
        // GET: GetData
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LoadFavoriteStt(string userId, string productId)
        {

            return Json(new { isFavorite = true });
        }
        public ActionResult ChangeFavoriteStt(string userId, string productId)
        {

            return Json(new { test = "" });
        }

    }

}