﻿
using DataService.Domain;
using DataService.Models.Entities;
using DataService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Unideli.ViewServices
{
    public class ProductServices
    {
        /// <summary>
        /// Get all product category active on brand and set display for website
        /// </summary>
        /// <returns></returns>
        public IQueryable<ProductCategoryViewModel> GetAllProductCategory()
        {
            EcomDomain productCategoryApi = new EcomDomain();
            IQueryable<ProductCategoryViewModel> listProductCategory = productCategoryApi.GetActiveDisplayWebsiteProductCategoryByBrand(TemplateConfig.BrandID);
            return listProductCategory;
        }
    }
}