﻿using DataService.Domain;
using DataService.Models.Entities;
using DataService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Unideli.ViewServices
{
    public class BlogCategoryViewService
    {
       public List<BlogCategoryViewModel> GetBlogCategoriesAsMenu()
        {
            TVFaceDomain domain = new TVFaceDomain();
            var result = domain.GetBlogCatesIsDisplay(TemplateConfig.BrandID).ToList();
            return result;
        }
    }
}