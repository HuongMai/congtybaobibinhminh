﻿using DataService.Domain;
using DataService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Unideli.Ultils.Enums;

namespace Unideli.ViewServices
{
    public class BlogPostViewService
    {
        public List<BlogPostViewModel> GetBlogCategoriesAsMenu()
        {
            var domain = new TVFaceDomain();
            var voteList = domain.GetAllBlogActivebyType((int)BlogTypeEnum.vote, TemplateConfig.BrandID).ToList();
            return voteList;
        }
    }
}