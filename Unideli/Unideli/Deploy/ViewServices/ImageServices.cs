﻿
using DataService.Domain;
using DataService.Models.Entities;
using DataService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Unideli.ViewServices
{
    public class ImageServices
    {
        /// <summary>
        /// Get all image in a image collection
        /// </summary>
        /// <param name="imageCollectionID">Image collection ID to get image</param>
        /// <returns></returns>
        public List<ImageCollectionItemViewModel> GetImageCollections(int imageCollectionID)
        {
            EcomDomain imageCollectionApi = new EcomDomain();
            List<ImageCollectionItemViewModel> listImage = imageCollectionApi.GetImageCollections(imageCollectionID);
            return listImage;
        }
    }
}