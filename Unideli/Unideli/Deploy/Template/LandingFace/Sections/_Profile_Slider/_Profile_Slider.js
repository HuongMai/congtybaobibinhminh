﻿var max = 200;var content = $('.ep-detail');
for (i = 0; i < content.length; i++) {
    if (content[i].textContent.length > max) {
        content[i].innerHTML = content[i].textContent.substring(0, max) + "...";
    };
};

$(document).ready(function () {
    $('.owl-four').owlCarousel({
        items: 5,
        loop: true,
        nav: true,
        dots: false,
        autoplay: true,
        autoplayTimeout: 2500,
        margin: 20,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 5
            }
        }
    })
})
