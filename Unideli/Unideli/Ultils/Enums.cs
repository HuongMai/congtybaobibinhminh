﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Unideli.Ultils
{
    public class Enums
    {
        public enum StatusOrder
        {
            New = 8,
            Processing = 10,
            Finish = 2,
            Cancel = 3,
        }
        public enum MenuUrlCode
        {
            FixUrl = 1,     //url chuyển đến 1 trang bất kỳ, ko nhất thiết phải thuộc store
            DevUrl = 2,     //url gồm Controller, Area, Action
            ReferenceUrl = 3,   //url chuyển đến 1 blog collection của store
        }
        public enum BlogCateTypeEnum
        {
            [Display(Name = "Tin tức")]
            News = 1,
            [Display(Name = "Video")]
            Video = 2,
            [Display(Name = "Sự kiện")]
            Event = 3,
            [Display(Name = "Resource")]
            Resource = 4,
            [Display(Name = "Khoá học")]
            Course = 8,
            [Display(Name = "Giảng viên")]
            Teacher = 9
        }
        public enum BlogTypeEnum
        {

            [Display(Name = "Tin tức")]
            News = 1,
            [Display(Name = "Video")]
            Video = 2,
            //[Display(Name = "Sự kiện")]
            //Event = 3,
            [Display(Name = "Resource")]
            Resource = 4,
            [Display(Name = "Quảng cáo")]
            Adsvertisement = 5,
            [Display(Name = "Background")]
            Background = 6,
            [Display(Name = "Logo")]
            Logo = 7,
            [Display(Name = "Khoá học")]
            Course = 8,
            [Display(Name = "Khách mời")]
            vote = 3

        }
        public enum BlogStatusEnum
        {
            [Display(Name = "Đang xử lý")]
            Processing = 0,
            [Display(Name = "Đã duyệt")]
            Approve = 1,
            [Display(Name = "Từ chối")]
            Reject = 2,
            [Display(Name = "Chờ đăng")]
            Scheduled = 3
        }

        public enum BlogTypeViewEnum
        {

            [Display(Name = "Tin tức")]
            News = 1,
            [Display(Name = "Khóa học")]
            Courses = 2,
            [Display(Name = "Giảng viên")]
            Teachers = 3
        }

        public enum MenuForRole
        {
            BrandAdminMenu = 1,
            StoreAdminMenu = 2,
            StoreWebMenu = 3
        }

        public enum MenuActionName
        {
            Admin = 0,
            Marketing = 1,
            Sales = 2,
            HR = 3,
            BlogPost = 4
        }

        public enum BirthdayOptionFilterEnum
        {
            ThisMonth = -1,
            BirthdayMonth = 1,
            BirthdayRange = 2
        }

        public enum SMSTypeEnum
        {
            [Display(Name = "Brand Name Quảng Cáo")]
            BrandNameCSKH = 2,
            [Display(Name = "Brand Name CSKH")]
            BrandNameAdvertysing = 1,
            [Display(Name = "Đầu số ngẫn nhiên")]
            RandomPhone = 3,
            [Display(Name = "Đầu số cố định")]
            FixPhone = 4,
            [Display(Name = "Đầu số 8755")]
            FixPhoneRegister = 6,
            [Display(Name = "Tốc độ cao đầu số ngẫu nhiên")]
            FastConnectRandomPhone = 7,
            [Display(Name = "Tốc độ cao đầu số cố định")]
            FastConnectFixPhone = 8
        }
        public enum CurrentStatusEnum
        {
            [Display(Name = "Tương lai")]
            Future = 0,
            [Display(Name = "Quá khứ")]
            Past = 1,
            [Display(Name = "Hiện tại")]
            Current = 3,
        }

        public enum StatusAttendance
        {
            [Display(Name = "Đi trễ")]
            ComeLate = 0,
            [Display(Name = "Về sớm")]
            ReturnEarly = 1,
            [Display(Name = "Đúng giờ")]
            OnTime = 3,
            [Display(Name = "Vắng")]
            Miss = 4,
            [Display(Name = "Vi phạm cả 2")]
            Bothviolate,
        }

        public enum RoomStatusEnum
        {
            Ready = 1,
            InUse = 2,
            Prepare = 3,
            NotUse = 4
        }

        public enum SessionStatusEnum
        {
            Open = 0,
            Close = 1,
            Modified = 2,
            Approve = 3,
            Empty = 4

        }

        public enum CostTypeEnum
        {
            [Display(Name = "Phiếu thu")]
            ReceiveCost = 1,
            [Display(Name = "Phiếu chi")]
            SpendingCost = 2,
            [Display(Name = "Phiếu thu xuất kho")]
            ReceiveCostTranferOut = 3,
            [Display(Name = "Phiếu chi nhập kho")]
            SpendingCostTranferIn = 4,
        }

        public enum ReceiveCostTypeEnum
        {
            [Display(Name = "Phiếu thu bình thường")]
            Normal = 1,
            [Display(Name = "Phiếu thu nợ")]
            PayDebt = 2,
            [Display(Name = "Phiếu thu khác")]
            Other = 3,
            [Display(Name = "Phiếu thu xuất kho")]
            OutInventory = 4,
        }
        public enum SpendingCostTypeEnum
        {
            [Display(Name = "Phiếu chi bình thường")]
            Normal = 1,
            [Display(Name = "Phiếu chi khác")]
            Other = 2,
            [Display(Name = "Phiếu chi nhập kho")]
            InInventory = 3,
        }

        public enum SaleTypeEnum
        {
            DefaultNothing = 0,
            DefaultAtStore = 1,
            DefaultTakeAway = 2,

        }

        public enum AdsStatusEnum
        {
            [Display(Name = "Hiển thị")]
            IsActive = 4,
            [Display(Name = "Dừng hiển thị")]
            NotActive = 5,

        }

        public enum RoomStateEnum
        {
            Ready = 1,
            RentHour = 2,
            RentNight = 3,
            RentDay = 4,
            Prepare = 5,
            NotUse = 6
        }



        /// <summary>
        /// Loai hoa don cho loai hinh Khach san -- Database: RentType
        /// </summary>
        public enum RentTypeEnum
        {
            Default = 0,
            Hour = 1,
            Night = 2,
            Day = 3,
            Addition = 5,
            Booking = 7,
            Drinking = 4,
        }


        /// <summary>
        /// Loại hóa đơn cho loại hình Cafe, Giao hàng --Database: OrderType 
        /// </summary>
        public enum OrderTypeEnum
        {
            [Display(Name = "Tại quán")]
            AtStore = 4,
            [Display(Name = "Mang đi")]
            TakeAway = 5,
            [Display(Name = "Giao hàng")]
            Delivery = 6,
            [Display(Name = "Bị hủy")]
            DropProduct = 8,
            [Display(Name = "Đặt hàng Online")]
            OnlineProduct = 1, // sản phẩm được đặt online
            [Display(Name = "Nạp thẻ")]
            OrderCard = 7
        }
    }
}