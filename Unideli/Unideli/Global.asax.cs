﻿using AutoMapper;
using DataService.Models.Entities;
using DataService.ViewModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Unideli
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            DataService.ApiEndpoint.Entry(this.AdditionalMapperConfig);
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("vi-VN");
        }
        public void AdditionalMapperConfig(IMapperConfigurationExpression config)
        {
            config.CreateMap<OrderViewModel, Order>();
            config.CreateMap<Order,OrderViewModel>();
            config.CreateMap<OrderDetailViewModel, OrderDetail>();
            config.CreateMap<OrderDetail, OrderDetailViewModel>();

            config.CreateMap<BlogPostCollectionViewModel, BlogPostCollection>();
            config.CreateMap<BlogPostCollection, BlogPostCollectionViewModel>();
            config.CreateMap<BlogPostCollectionItemMappingViewModel, BlogPostCollectionItemMapping>();
            config.CreateMap<BlogPostCollectionItemMapping, BlogPostCollectionItemMappingViewModel>();
            config.CreateMap<BlogPostViewModel, BlogPost>();
            config.CreateMap<BlogPost, BlogPostViewModel>();
        }

        protected void Application_Error()
        {
            Exception exception = Server.GetLastError();
            Response.Clear();

            HttpException httpException = exception as HttpException;

            if (httpException != null)
            {
                string action;

                switch (httpException.GetHttpCode())
                {
                    case 404:
                        // page not found
                        action = "~/khong-tim-thay";
                        break;
                    case 500:
                        // server error
                        action = "~/bao-tri";
                        break;
                    default:
                        action = "~/bao-tri";
                        break;
                }

                // clear error on server
                Server.ClearError();

                Response.Redirect(action);
            }
        }
    }
}
