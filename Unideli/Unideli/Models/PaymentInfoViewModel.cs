﻿
using DataService.Models.Entities;
using DataService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Unideli.Models
{
    public class PaymentInfoViewModel
    {   
        public OrderViewModel Order { get; set; }
        public List<OrderDetailViewModel> ListOrderDetail { get; set; }

        public PaymentInfoViewModel()
        {
            ListOrderDetail = new List<OrderDetailViewModel>();
        }
    }
}