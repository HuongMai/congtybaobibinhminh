﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataService.ViewModels;

namespace Unideli.Models
{
    public class CategoryViewModel
    {
        public List<SpecialProductCategoryViewModel> listCategory { get; set; }

        public CategoryViewModel()
        {
            listCategory = new List<SpecialProductCategoryViewModel>();
        }
    }
}