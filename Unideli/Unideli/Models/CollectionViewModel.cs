﻿using DataService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Unideli.Models
{
    public class CollectionViewModel
    {
        public List<SpecialProductCollectionViewModel> ListCollection { get; set; }

        public CollectionViewModel()
        {
            ListCollection = new List<SpecialProductCollectionViewModel>();
        }
    }
}