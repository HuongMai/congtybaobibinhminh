﻿using DataService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Unideli.Models
{
    public class HomeViewModel
    {
        public List<SpecialProductViewModel> ListTodayProduct { get; set; }
        //public List<SpecialProductCategoryViewModel> ListCategory { get; set; }
        public List<SpecialProductCollectionViewModel> ListCollection { get; set; }
        public BlogPostCollectionViewModel collection { get; set; }
        public IQueryable<BlogPostViewModel> blogPost { get; set; }

        public HomeViewModel()
        {
            ListTodayProduct = new List<SpecialProductViewModel>();
            //ListCategory = new List<SpecialProductCategoryViewModel>();
            ListCollection = new List<SpecialProductCollectionViewModel>();
        }
    }
}