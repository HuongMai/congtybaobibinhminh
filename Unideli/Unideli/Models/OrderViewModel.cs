﻿
using DataService.ViewModels;

namespace Unideli.Models
{
    public class OrderViewModel
    {
        public int TotalAmount { get; set; }
        public int Discount { get; set; }
        public int FinalAmount { get; set; }
        public int TotalQuantity { get; set; }
        public string DeliveryReceiver { get; set; }
        public string DeliveryAddress { get; set; }
        public string DeliveryPhone { get; set; }
        public string DeliveryEmail { get; set; }
        public List<SpecialOrderDetailViewModel> ListOrderDetail { get; set; }

        public OrderViewModel()
        {
            ListOrderDetail = new List<SpecialOrderDetailViewModel>();
        }
    }
}