﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataService.ViewModels;
namespace Unideli.Models
{
    public class ProductDetailViewModel
    {
        public SpecialProductViewModel productDetail;
        public SpecialProductCollectionViewModel collectionDetail;

        public ProductDetailViewModel()
        {
            productDetail = new SpecialProductViewModel();
            collectionDetail = new SpecialProductCollectionViewModel();
        }
    }
}