﻿using DataService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Unideli.Models
{
    public class VoteViewModel
    {
        public IQueryable<BlogPostViewModel> ListVoter { get; set; }
        public IQueryable<BlogPostCollectionItemMappingViewModel> ListVoted { get; set; }
    }
}