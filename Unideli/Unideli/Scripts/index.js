$('.choosen').click(function(e) {
    $('.choosen').removeClass('choosenElement');
    $('.choosen > a').removeClass('choosenElement');
    // e.preventDefault();
    $(this).addClass('choosenElement');
    $(this).children().addClass('choosenElement');
})
$('.swiper-wrapper .swiper-slide a').click(function() { //tạo sự kiện cho các thẻ a nằm trongD I header
    $('.current').removeClass("current"); //loại bỏ tất cả class current
    var posClick = $(this).attr('href'); //lấy giá trị trong thuộc tính href,gắn vào posClick. posClick sẽ có dạng #xxxxx
    var pos = $(posClick).offset().top; //lấy khoảng cách từ id #xxxxx tới đầu trang gắn vào pos
    console.log(pos);
    $("[href='" + posClick + "']").addClass("current"); //thêm class current vào thẻ có href bằng giá trị trong posClick
    $('html, body').animate({
        scrollTop: pos - 70 //lăn tới vị trí cách đầu trang 1 khoảng pos + 20 so với đầu trang
    }, 1500);
});

var swiper = new Swiper('.swiper-container', {
    slidesPerView: 10,
    spaceBetween: 5,
    loop: true,
    loopFillGroupWithBlank: true,
    breakpoints: {
        // when window width is <= 320px
        320: {
            slidesPerView: 1,
            spaceBetween: 10
        },
        // when window width is <= 480px
        480: {
            slidesPerView: 2,
            spaceBetween: 20
        },
        // when window width is <= 640px
        640: {
            slidesPerView: 3,
            spaceBetween: 30
        }
    },
    //   pagination: {
    //     el: '.swiper-pagination',
    //     clickable: true,
    //   },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
});

$(function(){
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {$(".menu").addClass("navbar-roll"); 
        $(".title").addClass("title-roll");
       // $(".special-li").addClass("special-li-roll");
    } 
    else {
        $(".navbar-roll").removeClass("navbar-roll");
        $(".title-roll").removeClass(".title-roll");
     //   $(".special-li-roll").removeClass("special-li-roll");
    } 
});
});