$(document).ready(function () {

    //MOBILE ONE AND MOBILE THREE
    $(".header .menu-toggle").click(function () {
        $('.mobile-nav').toggleClass('open-lg');
        $('.overlay').fadeToggle(300);
        if ($('.sidecart').hasClass('open-lg')) {
            $('.sidecart').removeClass('open-lg');
        }
    });
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $(".mobile-one").addClass("navbar-roll");
            $(".mobile-nav").addClass("mobile-nav-roll");
            $(".overlay").addClass("overlay-roll");
        } else {
            $(".navbar-roll").removeClass("navbar-roll");
            $(".mobile-nav-roll").removeClass("mobile-nav-roll");
            $(".overlay-roll").removeClass("overlay-roll");
        }
    });
    //custom cart animation
    $(".add-to-cart ").click(function () {
        if (!$('.sidecart').hasClass('open-lg')) {
            $('.sidecart').addClass('open-lg');
        }
    });
    
    $(".btn-accept").click(function () {
        if (!$('.sidecart').hasClass('open-lg')) {
            $('.sidecart').addClass('open-lg');
        }
    });
    $("#add-to-cart").click(function () {
        $('.sidecart').toggleClass('open-lg');
    });

});

//var app = angular.module("myApp", []);
//app.controller('myCtr', function($scope) {
//    $scope.name = "cuong";
//});


