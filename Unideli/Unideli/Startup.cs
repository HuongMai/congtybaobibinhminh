﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Unideli.Startup))]
namespace Unideli
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
