﻿var homeApp = angular.module("homeApp", ['commonUtils']);
var productCategoryApp = angular.module("productApp", ['commonUtils']);
var layoutCart = angular.module("layoutCart", ['commonUtils']);
var contactApp = angular.module("contactApp", ['commonUtils']);
var paymentApp = angular.module("paymentApp", ['commonUtils']);
var aboutusApp = angular.module("aboutusApp", ['commonUtils']);

//var test = function ();
var commonUtils = angular.module('commonUtils', []);
commonUtils.run(function ($rootScope, $http) {
    // using local storage
    $rootScope.numOfItems = 0;
    if ((localStorage.items === undefined) || localStorage.items === "") {
        $rootScope.cart = [];
    } else {
        eval(`var myObj = ${localStorage.items}`);
        $rootScope.cart = myObj;
        //  var storedNames = JSON.parse(localStorage.items);
        $rootScope.cart.forEach((arr) => {
            $rootScope.numOfItems += arr.count;
        });
    }
    if (localStorage.totalCart === undefined) {
        $rootScope.totalCart = 0;
    } else {
        $rootScope.totalCart = parseFloat(localStorage.totalCart);
        localStorage.totalCart = $rootScope.totalCart;
        $rootScope.discount = 10;
        localStorage.discount = $rootScope.discount;
        $rootScope.discountAmount = ($rootScope.discount / 100) * $rootScope.totalCart;
        localStorage.discountAmount = $rootScope.discountAmount;
        $rootScope.final = $rootScope.totalCart - $rootScope.discountAmount;
        localStorage.final = $rootScope.final;
    }
        $rootScope.addToCart = function (product, quantity) {
            let cartProduct = {};
            cartProduct.catID = product.catID;
            cartProduct.productID = product.productID;
            cartProduct.productName = product.productName;
            cartProduct.picURL = product.picURL;
            cartProduct.description = product.description;
            cartProduct.price = product.price;
            cartProduct.seoName = product.seoName;
            cartProduct.seoDecription = product.seoDecription;
            cartProduct.seoKeyWord = product.seoKeyWord;
            cartProduct.url = product.url;
            cartProduct.ExtraList = product.ExtraList;
            $rootScope.modalproduct = {};
            product = {};

            if ($rootScope.cart.length === 0) {
                cartProduct.count = parseInt(quantity);
                $rootScope.cart.push(cartProduct);
            } else {
                var repeat = false;
                for (var i = 0; i < $rootScope.cart.length; i++) {
                    if ($rootScope.cart[i].productID === cartProduct.productID) {
                        let compare = angular.equals($rootScope.cart[i].ExtraList, cartProduct.ExtraList);
                        if (compare) {
                            repeat = true;
                            //$rootScope.cart[i].count += parseInt($('#quantityCtrl').val());
                            $rootScope.cart[i].count += parseInt(quantity);
                        }

                    }
                }
                if (!repeat) {
                    //product.count = parseInt($('#quantityCtrl').val());
                    cartProduct.count = parseInt(quantity);
                    $rootScope.cart.push(cartProduct);
                }
            }
            $rootScope.totalCart += parseFloat(cartProduct.price * quantity);
            localStorage.totalCart = $rootScope.totalCart;
            $rootScope.discount = 10;
            localStorage.discount = $rootScope.discount;
            $rootScope.discountAmount = ($rootScope.discount / 100) * $rootScope.totalCart;
            localStorage.discountAmount = $rootScope.discountAmount;
            $rootScope.final = $rootScope.totalCart - $rootScope.discountAmount;
            localStorage.final = $rootScope.final;
            localStorage.items = JSON.stringify($rootScope.cart);
            $rootScope.numOfItems = 0;
            $rootScope.cart.forEach((arr) => {
                $rootScope.numOfItems += arr.count;
            });
            $('.sidecart').addClass('invisible-lg');
        }
    $rootScope.removeItemCart = function (product) {
        if (product.count > 1) {
            product.count -= 1;
            $rootScope.totalCart -= parseFloat(product.price);
        } else if (product.count === 1) {
            var index = $rootScope.cart.indexOf(product);
            $rootScope.cart.splice(index, 1);
            $rootScope.totalCart -= parseFloat(product.price);
        }
        $rootScope.discount = 10;
        localStorage.discount = $rootScope.discount;
        $rootScope.discountAmount = ($rootScope.discount / 100) * $rootScope.totalCart;
        localStorage.discountAmount = $rootScope.discountAmount;
        $rootScope.final = $rootScope.totalCart - $rootScope.discountAmount;
        localStorage.final = $rootScope.final;
        localStorage.totalCart = $rootScope.totalCart;
        localStorage.items = JSON.stringify($rootScope.cart);
        $rootScope.numOfItems = 0;
        $rootScope.cart.forEach((arr) => {
            $rootScope.numOfItems += arr.count;
        });
    };

    $rootScope.deleteItemCart = function (product) {
        for (var i = 0; i < $rootScope.cart.length; i++) {
            if ($rootScope.cart[i].productID === product.productID) {
                $rootScope.totalCart -= parseFloat(product.price * product.count);
                localStorage.totalCart = $rootScope.totalCart;
                $rootScope.discount = 10;
                localStorage.discount = $rootScope.discount;
                $rootScope.discountAmount = ($rootScope.discount / 100) * $rootScope.totalCart;
                localStorage.discountAmount = $rootScope.discountAmount;
                $rootScope.final = $rootScope.totalCart - $rootScope.discountAmount;
                localStorage.final = $rootScope.final;
                $rootScope.cart.splice(i, 1);
                break;
            }
        }
        localStorage.totalCart = $rootScope.totalCart;
        localStorage.items = JSON.stringify($rootScope.cart);
        $rootScope.numOfItems = 0;
        $rootScope.cart.forEach((arr) => {
            $rootScope.numOfItems += arr.count;
        });
    };

    $rootScope.modalproduct = {}; //product displayed on modal
    $rootScope.extras = []; // extra list to display on modal
    $rootScope.addPriceOrNot = []; // boolean variable array to know whether add extra to total or not
    $rootScope.addToModal = function (product) {
        //append product to modal producot in order to display on modal
        $rootScope.quantity = 1;
        $rootScope.modalproduct.catID = product.catID;
        $rootScope.modalproduct.productID = product.productID;
        $rootScope.modalproduct.productName = product.productName;
        $rootScope.modalproduct.picURL = product.picURL;
        $rootScope.modalproduct.description = product.description;
        $rootScope.modalproduct.price = product.price;
        $rootScope.modalproduct.seoName = product.seoName;
        $rootScope.modalproduct.seoDecription = product.seoDecription;
        $rootScope.modalproduct.seoKeyWord = product.seoKeyWord;
        $rootScope.modalproduct.url = product.url;
        $rootScope.modalproduct.ExtraList = [];

        // get extra of category by categoryID
        $http({
            method: 'GET',
            url: 'http://localhost:46209/getExtraProduct?ParentCatId=' + product.catID
        }).then(function (response) {
            $rootScope.extras = response.data;
            for (var i = 0; i < $rootScope.extras.length; i++) {
                $rootScope.addPriceOrNot[i] = false;
            }
        }, function (response) {
            console.log('Fail');
        });
    };

    $rootScope.addExtra = function (extra, index) {
        if ($rootScope.addPriceOrNot[index]) {
            $rootScope.modalproduct.price += extra.Price;
            $rootScope.modalproduct.ExtraList.push(extra);
        }
        else {
            $rootScope.modalproduct.price -= extra.Price;
            let index = $rootScope.modalproduct.ExtraList.indexOf(extra);
            $rootScope.modalproduct.ExtraList.splice(index, 1);
        }
    };




});





