﻿var homeApp = angular.module("homeApp", ['commonUtils']);
var productCategoryApp = angular.module("productApp", ['commonUtils']);
var layoutCart = angular.module("layoutCart", ['commonUtils']);
var contactApp = angular.module("contactApp", ['commonUtils']);
var paymentApp = angular.module("paymentApp", ['commonUtils']);
var aboutusApp = angular.module("aboutusApp", ['commonUtils']);

//var test = function ();
var commonUtils = angular.module('commonUtils', []);
commonUtils.run(function ($rootScope, $http) {
    // using local storage
    $rootScope.numOfItems = 0;
    if ((localStorage.items === undefined) || localStorage.items === "") {
        $rootScope.cart = [];
    } else {
        eval(`var myObj = ${localStorage.items}`);
        $rootScope.cart = myObj;
        //  var storedNames = JSON.parse(localStorage.items);
        //  console.log(storedNames);
        $rootScope.cart.forEach((arr) => {
            $rootScope.numOfItems += arr.count;
        });
    }
    if (localStorage.totalCart === undefined) {
        $rootScope.totalCart = 0;
    } else {
        $rootScope.totalCart = parseFloat(localStorage.totalCart);
        localStorage.totalCart = $rootScope.totalCart;
        $rootScope.discount = 10;
        localStorage.discount = $rootScope.discount;
        $rootScope.final = $rootScope.totalCart - (($rootScope.discount / 100) * $rootScope.totalCart);
        localStorage.final = $rootScope.final;
    }
    $rootScope.addToCart = function (product) {
        if ($rootScope.cart.length === 0) {
            product.count = 1;
            $rootScope.cart.push(product);
        } else {
            var repeat = false;
            for (var i = 0; i < $rootScope.cart.length; i++) {
                if ($rootScope.cart[i].productID === product.productID) {
                    repeat = true;
                    $rootScope.cart[i].count += 1;
                }
            }
            if (!repeat) {
                product.count = 1;
                $rootScope.cart.push(product);
            }
        }
        $rootScope.totalCart += parseFloat(product.price);
        localStorage.totalCart = $rootScope.totalCart;
        $rootScope.discount = 10;
        localStorage.discount = $rootScope.discount;
        $rootScope.final = $rootScope.totalCart - (($rootScope.discount / 100) * $rootScope.totalCart);
        localStorage.final = $rootScope.final;
        // console.log(typeof $rootScope.cart);
        localStorage.items = JSON.stringify($rootScope.cart);
        $rootScope.numOfItems = 0;
        $rootScope.cart.forEach((arr) => {
            $rootScope.numOfItems += arr.count;
        });
        $('.sidecart').addClass('invisible-lg');
    }
    $rootScope.removeItemCart = function (product) {
        if (product.count > 1) {
            product.count -= 1;
            $rootScope.totalCart -= parseFloat(product.price);
        } else if (product.count === 1) {
            var index = $rootScope.cart.indexOf(product);
            $rootScope.cart.splice(index, 1);
            $rootScope.totalCart -= parseFloat(product.price);
        }
        $rootScope.discount = 10;
        localStorage.discount = $rootScope.discount;
        $rootScope.final = $rootScope.totalCart - (($rootScope.discount / 100) * $rootScope.totalCart);
        localStorage.final = $rootScope.final;
        localStorage.totalCart = $rootScope.totalCart;
        localStorage.items = JSON.stringify($rootScope.cart);
        $rootScope.numOfItems = 0;
        $rootScope.cart.forEach((arr) => {
            $rootScope.numOfItems += arr.count;
        });
    };

    $rootScope.deleteItemCart = function (product) {
        for (var i = 0; i < $rootScope.cart.length; i++) {
            if ($rootScope.cart[i].productID === product.productID) {
                $rootScope.totalCart -= parseFloat(product.price * product.count);
                localStorage.totalCart = $rootScope.totalCart;
                $rootScope.discount = 10;
                localStorage.discount = $rootScope.discount;
                $rootScope.final = $rootScope.totalCart - (($rootScope.discount / 100) * $rootScope.totalCart);
                localStorage.final = $rootScope.final;
                $rootScope.cart.splice(i, 1);
                break;
            }
        }
        localStorage.totalCart = $rootScope.totalCart;
        localStorage.items = JSON.stringify($rootScope.cart);
        $rootScope.numOfItems = 0;
        $rootScope.cart.forEach((arr) => {
            $rootScope.numOfItems += arr.count;
        });
    };

    $rootScope.modalproduct = {}; //product displayed on modal
    $rootScope.extras = []; // extra list to display on modal
    $rootScope.addPriceOrNot = []; // boolean variable array to know whether add extra to total or not
    $rootScope.addToModal = function (product) {
        //append product to modal product in order to display on modal
        $rootScope.modalproduct.catID = product.catID;
        $rootScope.modalproduct.productID = product.productID;
        $rootScope.modalproduct.productName = product.productName;
        $rootScope.modalproduct.picURL = product.picURL;
        $rootScope.modalproduct.description = product.description;
        $rootScope.modalproduct.price = product.price;
        $rootScope.modalproduct.seoName = product.seoName;
        $rootScope.modalproduct.seoDecription = product.seoDecription;
        $rootScope.modalproduct.seoKeyWord = product.seoKeyWord;
        $rootScope.modalproduct.url = product.url;

        // get extra of category by categoryID
        $http({
            method: 'GET',
            url: 'getExtraProduct?ParentCatId=' + product.catID
        }).then(function (response) {
            $rootScope.extras = response.data;
            for (var i = 0; i < $rootScope.extras.length; i++) {
                $rootScope.addPriceOrNot[i] = false;
            }
        }, function (response) {
            console.log('Fail');
        });
    };
    
    $rootScope.addExtraPrice = function (price, index) {
        if ($rootScope.addPriceOrNot[index]) {
            $rootScope.modalproduct.price += price;
            $rootScope.addPriceOrNot[index] = true;
        }
        else {
            $rootScope.addPriceOrNot[index] = false;
            $rootScope.modalproduct.price -= price;
        }

    };




});





