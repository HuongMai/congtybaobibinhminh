﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace Unideli
{
    public static class SectionHelper
    {
        /// <summary>
        /// Html Helper render partial view by input partial name
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="partialName">Partial name</param>
        /// <returns></returns>
        public static MvcHtmlString Section(this HtmlHelper helper, string partialName)
        {
            return helper.Partial(string.Format("~/Template/{0}/Sections/{1}/{1}.cshtml", TemplateConfig.TemplateName, partialName));
        }

        /// <summary>
        /// Html Helper render partial view by input partial name and model
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="partialName">Partial name</param>
        /// <param name="model">Model</param>
        /// <returns></returns>
        public static MvcHtmlString Section(this HtmlHelper helper, string partialName, object model)
        {
            return helper.Partial(string.Format("~/Template/{0}/Sections/{1}/{1}.cshtml", TemplateConfig.TemplateName, partialName), model);
        }
    }
}