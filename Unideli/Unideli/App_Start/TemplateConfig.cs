﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Unideli
{
    public static class TemplateConfig
    {
        /// <summary>
        /// Layout file path
        /// </summary>
        public static string LayoutPath = "~/Template/BaoBiNhuaBinhMinh/Layouts/_Layout.cshtml";

        /// <summary>
        /// Template folder name
        /// </summary>
        public static string TemplateName = "BaoBiNhuaBinhMinh";

        /// <summary>
        /// StoreID of Website
        /// </summary>
        //public static int BrandID = 4;
        public static int BrandID = 1;

        /// <summary>
        /// StoreID of Website
        /// </summary>
        public static int StoreID = 62;
        //public static int StoreID = 2;

        public static string ImageDomain = "http://admin.guongmattruyenhinh.com";
    }
}