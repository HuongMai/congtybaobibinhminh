﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace Unideli.App_Start
{
    public class CategoryRouteConstraint : IRouteConstraint
    {
        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            var productCategory = new ProductCategoryApi();
            var categories = productCategory.GetAllRoute();

            if (values[parameterName] == null)
                return false;
            var category = values[parameterName].ToString();

            return categories.ContainsKey(category);
        }
    }
}