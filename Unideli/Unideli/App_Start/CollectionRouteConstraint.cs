﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace Unideli.App_Start
{
    public class CollectionRouteConstraint : IRouteConstraint
    {
        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            var productCollection = new ProductCollectionApi();
             var collections = productCollection.GetAllRoute();

            if (values[parameterName] == null)
                return false;
            var collection = values[parameterName].ToString();

            return collections.ContainsKey(collection);
        }
    }
}