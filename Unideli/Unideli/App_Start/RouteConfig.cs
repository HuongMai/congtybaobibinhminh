﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;


namespace Unideli
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
                name: "HomePage_Default",
                url: "",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
                );
            routes.MapRoute(
                name: "HomePage",
                url: "trang-chu",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
                );
            routes.MapRoute(
               name: "AboutUs",
               url: "gioi-thieu",
               defaults: new { controller = "AboutUs", action = "Index" }
               );

            routes.MapRoute(
               name: "Ability",
               url: "nang-luc",
               defaults: new { controller = "Ability", action = "Index" }
               );

            routes.MapRoute(
         name: "Machines",
         url: "may-moc",
         defaults: new { controller = "Machines", action = "Index" }
         );

            routes.MapRoute(
       name: "Factory",
       url: "nha-may",
       defaults: new { controller = "Factory", action = "Index" }
       );
            routes.MapRoute(
name: "Materials",
url: "vat-lieu",
defaults: new { controller = "Materials", action = "Index" }
);

            routes.MapRoute(
name: "Quality",
url: "chat-luong",
defaults: new { controller = "Quality", action = "Index" }
);

            routes.MapRoute(
             name: "Sales_Marketing",
             url: "sale-marketing",
             defaults: new { controller = "Sales_Marketing", action = "Index" }
             );

            routes.MapRoute(
             name: "Recruitment",
             url: "nhanvien-KCS",
             defaults: new { controller = "Recruitment", action = "Index" }
             );

            routes.MapRoute(
           name: "Engineering",
           url: "nhanvien-kythuat",
           defaults: new { controller = "Engineering", action = "Index" }
           );


            routes.MapRoute(
                name: "Blog",
                url: "tin-tuc",
                defaults: new { controller = "Blog", action = "Index" }
                );
            routes.MapRoute(
               name: "BlogDetail",
               url: "tin-tuc/{seoname}",
               defaults: new { controller = "Blog", action = "BlogDetail", seoname = UrlParameter.Optional }
               );
            routes.MapRoute(
                name: "Account",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Account", action = "Login", id = UrlParameter.Optional }
            );


            //routes.MapRoute(
            //    name: "Default",
            //    url: "{controller}/{action}/{id}",
            //    defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            //);
        }
    }
}
