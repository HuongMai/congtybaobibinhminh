﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace Unideli.App_Start
{
    public class ProductRouteConstraint : IRouteConstraint
    {
        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            var productApi = new ProductApi();
            var products = productApi.GetAllRoute();

            if (values[parameterName] == null)
                return false;

            var product = values[parameterName].ToString();

            return products.ContainsKey(product);
        }
    }
}