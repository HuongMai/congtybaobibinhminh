﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace Unideli.App_Start
{
    public class StaticPageRouteConstraint : IRouteConstraint
    {
        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            var webPageApi = new WebPageApi();
            var WebPages = webPageApi.GetAllRoute();

            if (values[parameterName] == null)
                return false;

            var webPage = values[parameterName].ToString();

            return WebPages.ContainsKey(webPage);
        }
    }
}